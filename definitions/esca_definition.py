from model.user_definition import UserDefinition

definition = UserDefinition("Carlos", "esca@email.service.com", preferred_ice_cream="Bariloche's Chocolate")

if __name__ == '__main__':
    profile = definition
    print(f"This is all information that we know about '{profile.name}'")
    profile.describe()
