from definition import user_instances


def show_user(user_profile):
    user_profile.describe(2)


if __name__ == '__main__':

    print(f'This is all the information that we know about ours teammates...!')

    for i, user in enumerate(user_instances):
        print(f'    Teammate number #{i+1}!')
        show_user(user)
