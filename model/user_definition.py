class UserDefinition:

    def __init__(self, name, email, hobbies="Sleep All Day", preferred_ice_cream="No One", **kwargs):
        self._name = name
        self._email = email
        self._hobbies = hobbies
        self._preferred_ice_cream = preferred_ice_cream
        self._extra_properties = kwargs

    @property
    def name(self):
        return self._name

    def __iter__(self):
        yield "User Name", self._name
        yield "Email Address", self._email
        yield "Hobbies", self._hobbies
        yield "Most loved ice cream", self._preferred_ice_cream

        for property_name, property_value in self._extra_properties:
            yield property_name, property_value

    def describe(self, indent=1):
        for property_name, property_value in self:
            print(f"{'    ' * indent}{property_name}:'{property_value}'")


if __name__ == '__main__':
    profile = UserDefinition("Example User", "user.fake@fake.service.com")
    print(f"This is all information that we know about '{profile.name}'")
    profile.describe()
